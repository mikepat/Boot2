package com.cds.boot.Boot2;

public class DataHolder {
	public String name;
	public String luckyNumber;
	
	public DataHolder() {
		this.name = "";
		this.luckyNumber = "";
	}
	@Override
	public String toString() {
		return this.name + " " + this.luckyNumber;
	}
}
