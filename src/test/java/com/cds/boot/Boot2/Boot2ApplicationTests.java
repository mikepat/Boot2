package com.cds.boot.Boot2;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Boot2ApplicationTests {

	@Autowired WebApplicationContext ctx;
	
	private static final Logger LOG = LoggerFactory.getLogger(Boot2ApplicationTests.class);
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetString() throws Exception {
		
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		MvcResult result = mvc.perform(get("/string")).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		LOG.debug(response);
		assertThat(response).isEqualTo("Hello, World");
	}
	
	@Test
	public void testGetStringWithName() throws Exception {
		
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		MvcResult result = mvc.perform(get("/string?name=mike")).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		LOG.debug(response);
		assertThat(response).isEqualTo("Hello, mike");
	}
	
	@Test
	public void testGetView() throws Exception {
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		mvc.perform(post("/view?name=mike")).andExpect(status().isOk()).andExpect(view().name("home"))
				.andExpect(model().attribute("name", "mike"))
				.andReturn();
	}
}
