package com.cds.boot.Boot2.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cds.boot.Boot2.DataHolder;

@RestController
public class FirstController {

	private static final Logger LOG = LoggerFactory.getLogger(FirstController.class);
	
	@RequestMapping(path="/string")
	public String returnString(@RequestParam(required=false) String name) {
		LOG.debug("Hello from Logback");
		if (name == null) {
			return "Hello, World";
		} else {
			return "Hello, " + name;
		}
	}
	
	@RequestMapping(path="/json", produces="application/json")
	public DataHolder returnJson() {
		DataHolder dh = new DataHolder();
		dh.name = "Mike";
		dh.luckyNumber = "123";
		return dh;
	}
	
	@RequestMapping(path="/post", method=RequestMethod.POST)
	public String testPosting(@RequestBody DataHolder dh) {
		
		LOG.error(dh.toString());
		return "success";
	}
}
