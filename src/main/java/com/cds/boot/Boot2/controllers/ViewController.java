package com.cds.boot.Boot2.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

	@RequestMapping(path="/view")
	public ModelAndView renderView(@RequestParam String name) {
		
		ModelAndView mav = new ModelAndView("home");
		mav.addObject("name", name);
		return mav;
	}
}
